package edu.illinois.ngirdha2.finalproj;

import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.LinkedList;

import static java.lang.Math.abs;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    /*private SensorManager mGyroSensorManager;
    private SensorManager mMagSensorManager;
    private SensorManager mLightSensorManager;*/
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakeLock;

    private Sensor accelerometer;
    private Sensor gyroscope;
    private Sensor magnetometer;
    private Sensor lightsensor;
    private Sensor orientation;

    private float deltaX, deltaY, deltaZ; //Accel variables
    private float g_X, g_Y, g_Z; //Gyroscope variables
    private float old_g_X = 0; private float AnglesumX = 0;
    private float old_g_Y = 0; private float AnglesumY = 0;
    private float old_g_Z = 0; private float AnglesumZ = 0;

    private float orientangle_XY = 0;
    private float orientangle_Z = 0;
    private float rotationX = 0;
    private float rotationY = 0;
    private float rotationZ = 0;
    private float bearing = 0;


    private float mag_X, mag_Y, mag_Z; //Magnetometer variables
    private float light_val; //Light variable
    long startTime = System.currentTimeMillis();
    long currTime;
    int stepCount = 0;
    boolean Accelhigh = false;

    /*private TextView AccelX, AccelY, AccelZ, GyroX, GyroY, GyroZ, MagX, MagY, MagZ, Light1, stepLabel;
    private TextView AngleX, AngleY, AngleZ, dist;*/



    private TextView orientangleXY, orientangleZ;
    private TextView objectTag,objectTag2,objectTag3;


    // Create a constant to convert nanoseconds to seconds.
    /*private static final float NanoS2S = 1.0f / 1000000000.0f;
    private static final float MilliS2S = 1.0f / 1000.0f;
    private final float[] deltaRotationVector = new float[4];*/
    private float Gyrotimestamp;
    //float[] deltaRotationMatrix = new float[9];

    private Camera mCamera = null;
    private CameraView mCameraView = null;

    private LinkedList ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeViews();
        setupCameraView();
        // create a linked list
        ll = new LinkedList<Node>();

        //Outside of the screen
        objectTag.setX(2000);
        objectTag.setY(2000);

        objectTag2.setX(2000);
        objectTag2.setY(2000);

        objectTag3.setX(2000);
        objectTag3.setY(2000);

        orientangleXY.setY(200);

        Toast.makeText(MainActivity.this, "Hello!!", Toast.LENGTH_LONG).show();

        // Get an instance of the SensorManager
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);


        gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_UI);

        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);
        sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);

        lightsensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        sensorManager.registerListener(this, lightsensor, SensorManager.SENSOR_DELAY_UI);

        orientation = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        sensorManager.registerListener(this, orientation, SensorManager.SENSOR_DELAY_UI);

        // Create a bright wake lock
        //mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, getClass()
        //        .getName());


        Button btnChangeText = (Button) findViewById(R.id.btnChangeText);
        btnChangeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertLinkedList();
                //final TextView textView = (TextView) findViewById(R.id.textview1);
                //textView.setText("Saving!");
                new CountDownTimer(1000,1000) {
                    public void onTick(long millisUntilFinished) {}
                    public void onFinish() {
                     //   textView.setText("done!");
                    }
                }.start();
            }
        });
    }

    public void insertLinkedList(){
        ll.add(new Node(orientangle_XY, orientangle_Z, bearing,"A"));
        //ll.add(new Point(1,1));
        //ll.add(new Point(5,5));
    }

    public void displayLinkedList() {
        for(int i=0; i < ll.size(); i++){
            Node node1 = (Node) ll.get(i);
            Float xy = node1.orientangle_XY;
            Float z = node1.orientangle_Z;
            String tag = node1.tag;

            //Make new Text views here and set their x and y locations from the node1.orientagle_XY and Z
            //Then make their text equal to tag





            if( (bearing  <= node1.bearing + 25) && (bearing >= node1.bearing - 25)){

                switch(i & 3){
                    case 0:
                        if(bearing < node1.bearing  )//Moved left
                            objectTag.setX(450 + 110*abs(node1.orientangle_XY - orientangle_XY));
                        else
                            objectTag.setX(450 - 110*abs(node1.orientangle_XY - orientangle_XY));
                        break;

                    case 1:
                        if(bearing < node1.bearing  )//Moved left
                            objectTag2.setX(450 + 110*abs(node1.orientangle_XY - orientangle_XY));
                        else
                            objectTag2.setX(450 - 110*abs(node1.orientangle_XY - orientangle_XY));

                        break;

                    case 2:
                        if(bearing < node1.bearing  )//Moved left
                            objectTag3.setX(450 + 110*abs(node1.orientangle_XY - orientangle_XY));
                        else
                            objectTag3.setX(450 - 110*abs(node1.orientangle_XY - orientangle_XY));

                        break;

                    default :
                        break;
                }


            }
            else{
                switch (i % 3){
                    case 0:
                        objectTag.setX(2000);
                        break;
                    case 1:
                        objectTag2.setX(2000);
                        break;
                    case 2:
                        objectTag3.setX(2000);
                        break;
                    default :
                        break;

                }
            }


            if((orientangle_Z <= node1.orientangle_Z + 30) && (orientangle_Z >= node1.orientangle_Z - 30)) {

                switch(i % 3) {
                    case 0:
                        objectTag.setY(700 + 25*(orientangle_Z - node1.orientangle_Z));
                        break;
                    case 1:
                        objectTag2.setY(700 + 25 * (orientangle_Z - node1.orientangle_Z));
                        break;
                    case 2:
                        objectTag3.setY(700 + 25 * (orientangle_Z - node1.orientangle_Z));
                        break;
                    default:
                        break;
                }
            }
        }

    }

    public void setupCameraView() {
        try{
            mCamera = Camera.open();//you can use open(int) to use different cameras
        } catch (Exception e){
            Log.d("ERROR", "Failed to get camera: " + e.getMessage());
        }

        if(mCamera != null) {
            mCameraView = new CameraView(this, mCamera);//create a SurfaceView to show camera data
            FrameLayout camera_view = (FrameLayout)findViewById(R.id.camera_view);
            camera_view.addView(mCameraView);//add the SurfaceView to the layout
        }
    }

    public void initializeViews() {
        /*AccelX = (TextView) findViewById(R.id.AccelX);
        AccelY = (TextView) findViewById(R.id.AccelY);
        AccelZ = (TextView) findViewById(R.id.AccelZ);
        GyroX = (TextView) findViewById(R.id.GyroX);
        GyroY = (TextView) findViewById(R.id.GyroY);
        GyroZ = (TextView) findViewById(R.id.GyroZ);
        MagX = (TextView) findViewById(R.id.MagX);
        MagY = (TextView) findViewById(R.id.MagY);
        MagZ = (TextView) findViewById(R.id.MagZ);
        Light1 = (TextView) findViewById(R.id.Light1);
        stepLabel = (TextView) findViewById(R.id.stepCount);
        AngleX = (TextView) findViewById(R.id.AngleX);
        AngleY = (TextView) findViewById(R.id.AngleY);
        AngleZ = (TextView) findViewById(R.id.AngleZ);
        dist = (TextView) findViewById(R.id.dist);*/
        orientangleXY = (TextView) findViewById(R.id.orientangleXY);
        orientangleZ = (TextView) findViewById(R.id.orientangleZ);
        objectTag = (TextView) findViewById(R.id.object_tag);
        objectTag2 = (TextView) findViewById(R.id.object_tag2);
        objectTag3 = (TextView) findViewById(R.id.object_tag3);
    }

    //onResume() register the accelerometer for listening the events
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, lightsensor, SensorManager.SENSOR_DELAY_UI);

    }

    //onPause() unregister the accelerometer for stop listening the events
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        currTime = System.currentTimeMillis() - startTime; //capture timedifference
        Sensor sensor = event.sensor;


        if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            // get the change of the x,y,z values of the accelerometer
            deltaX = event.values[0];
            deltaY = event.values[1];
            deltaZ = event.values[2];

            orientationAnglecalc();
            displayLinkedList();


            //Stepcount algorithmn
            if (deltaZ > 10.5 && Accelhigh == false) {
                stepCount++; Accelhigh = true;
            } else if (deltaZ < 9 && Accelhigh == true) {
                Accelhigh = false;
            }

            // display the current x,y,z accelerometer values
            displayCurrentValues();
        }else if (sensor.getType() == Sensor.TYPE_ORIENTATION){
            float degree = Math.round(event.values[0]);
            bearing = degree;

            if(bearing > 180)
                bearing = -1 *(360 - bearing);


        }
        else if (sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            // get the change of the x,y,z values of the gyroscope
            g_X = event.values[0];
            g_Y = event.values[1];
            g_Z = event.values[2];

            AngleSum();
            displayCurrentValues();
        } else if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
            // get the change of the x,y,z values of the magnetometer
            mag_X = event.values[0];
            mag_Y = event.values[1];
            mag_Z = event.values[2];


            orientationAnglecalc();
            displayCurrentValues();
        } else if (sensor.getType() == Sensor.TYPE_LIGHT) {
            // get the change of the x,y,z values of the magnetometer
            light_val = event.values[0];
            displayCurrentValues();
        }
        // clean current values
        //displayCleanValues();

    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            //TextView textView = (TextView) findViewById(R.id.textView6);
            //textView.setText("External Allowed");
            return true;
        }
        //TextView textView = (TextView) findViewById(R.id.textView6);
        //textView.setText("External NotAllowed");
        return false;
    }

    private void AngleSum(){
        if (Gyrotimestamp != 0) {
            final float dT = (currTime - Gyrotimestamp) / 1000.0f;
            if(abs(g_X) > 0.50f) {
                rotationX = abs(dT * (g_X + old_g_X) / 2 * (180 / 3.14f));
                AnglesumX += rotationX;
            }
            if(abs(g_Y) > 0.70f) {
                rotationY = abs(dT * (g_Y + old_g_Y) / 2 * (180 / 3.14f));
                AnglesumY += rotationY;

            }
            if(abs(g_Z) > 0.5f) {
                rotationZ = abs(dT * (g_Z + old_g_Z) / 2 * (180 / 3.14f));
                AnglesumZ += rotationZ;

            }



        }
        Gyrotimestamp = currTime;
        old_g_X = g_X;
        old_g_Y = g_Y;
        old_g_Z = g_Z;
    }

    //x/y
    private void orientationAnglecalc() {
        orientangle_XY = (float) Math.atan(mag_X / mag_Z) * 360 / (2 * 3.14f);
        orientangle_Z = (float) Math.acos(deltaZ / 9.67f) * 360 / (2 * 3.14f);
        if (orientangle_Z != orientangle_Z) // check for Not a Number
            orientangle_Z = 0.0f;
    }




    private void appendToFile(String str) {
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/namit");
            myDir.mkdirs();
            File file = getFileStreamPath("test.txt");
            if (!file.exists()) {
                file.createNewFile();
            }

            FileOutputStream writer = openFileOutput(file.getName(), MODE_APPEND | MODE_WORLD_READABLE);
            writer.write(str.getBytes());
            writer.flush();
            writer.close();
        } catch (java.io.IOException ioe) {
            ioe.printStackTrace();
        }
    }

    // display the current x,y,z accelerometer values
    public void displayCurrentValues() {
       /* TextView textView = (TextView) findViewById(R.id.time_label);
        textView.setText(Long.toString(currTime));
        AccelX.setText(String.format("%.4f", deltaX)); //Float.toString(deltaX));
        AccelY.setText(String.format("%.4f", deltaY));
        AccelZ.setText(String.format("%.4f", deltaZ));
        GyroX.setText(String.format("%.4f", g_X));
        GyroY.setText(String.format("%.4f", g_Y));
        GyroZ.setText(String.format("%.4f", g_Z));
        MagX.setText(String.format("%.3f", mag_X));
        MagY.setText(String.format("%.3f", mag_Y));
        MagZ.setText(String.format("%.3f", mag_Z));
        Light1.setText(Float.toString(light_val));
        dist.setText(Float.toString(stepCount * 0.35f));
        stepLabel.setText(Integer.toString(stepCount));
        DecimalFormat df = new DecimalFormat("##.##");
        AngleX.setText(df.format(AnglesumX));
        AngleY.setText(new DecimalFormat("##.##").format(AnglesumY));
        AngleZ.setText(Float.toString(AnglesumZ));


        orientangleZ.setText(Float.toString(orientangle_Z));
        //AngleX.setText(df.format(deltaRotationVector[0]));
        //AngleY.setText(new DecimalFormat("##.##").format(deltaRotationVector[1]));
        //AngleZ.setText(Float.toString(deltaRotationVector[2]));*/

        //isExternalStorageWritable();
        orientangleXY.setText(Float.toString(bearing));
        orientangleZ.setText(Float.toString(orientangle_Z));
        //to generate the CSV
        String filename = "results.csv";
        try {
            String ResultString="";
            ResultString += Long.toString(currTime) + ",";
            ResultString += String.format("%.4f", deltaX) + ",";
            ResultString += String.format("%.4f", deltaY) + ",";
            ResultString += String.format("%.4f", deltaZ) + ",";
            ResultString += String.format("%.4f", g_X) + ",";
            ResultString += String.format("%.4f", g_Y) + ",";
            ResultString += String.format("%.4f", g_Z) + ",";
            ResultString += String.format("%.3f", mag_X) + ",";
            ResultString += String.format("%.3f", mag_Y) + ",";
            ResultString += String.format("%.3f", mag_Z) + ",";
            ResultString += Float.toString(light_val) + "\n";

            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/namit");
            if (!myDir.exists())
                myDir.mkdir();
            Log.v("Directory:", myDir.toString());

            /*if (!myDir.mkdirs()) {
                Log.v("Directory","not created");
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "namit");
            if (file.exists())
                Log.v("Folder"," exists");
            else if (!file.mkdirs()) {
                Log.v("Directory"," not created");
            }*/
            File file = new File(myDir, filename);
            if (!file.exists()) {
                file.createNewFile();
                //Log.v("File:",file.toString());
                FileOutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter osw = new OutputStreamWriter(fOut);
                osw.write("Timestamp(ms),Accel_x,Accel_y,Accel_z,Gyro_x,Gyro_y,Gyro_z,Mag_x,Mag_y,Mag_z,Light\n");
                osw.flush();
                osw.close();
            }
            //if (file.exists ()) file.delete ();

            //FileOutputStream writer = openFileOutput(file.getName(), MODE_APPEND);
            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);

            // Write the string to the file
            /*for(int i=1; i< total_row; i++)
            {

                for(int j=1; j< total_col; j++)
                {
                    TestString += table[i][j].getText().toString();        // to pass in every widget a context of activity (necessary)
                    TestString += " ,";
                }
                TestString+="\n";
            }*/
            Log.v("the string is", ResultString);
            osw.write(ResultString);
            osw.flush();
            osw.close();
            //appendToFile(ResultString);
        } catch (java.io.IOException ioe)
        { ioe.printStackTrace(); }

    }
}
