package edu.illinois.ngirdha2.finalproj;

import android.widget.TextView;

/**
 * Created by namg on 12/1/16.
 */
public class Node {
    public Float orientangle_XY;
    public Float bearing;
    public Float orientangle_Z;
    public String tag = "";
    //constructor
    public Node(Float a, Float b, Float c, String d){
        orientangle_XY = a;
        orientangle_Z = b;
        bearing = c;
        tag = d;
    }
}
